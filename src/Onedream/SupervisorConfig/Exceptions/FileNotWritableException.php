<?php

namespace Onedream\SupervisorConfig\Exceptions;

use Exception;

class FileNotWritableException extends Exception {}