<?php

namespace Onedream\SupervisorConfig\Exceptions;

use Exception;

class InvalidArgumentsException extends Exception {}