<?php

namespace Onedream\SupervisorConfig;

use File;
use Onedream\SupervisorConfig\Exceptions\FileNotWritableException;
use Onedream\SupervisorConfig\Exceptions\InvalidArgumentsException;

class Generator
{

    /**
     * Name of the program.
     * @var string
     */
    private $name;

    /**
     * The command to run.
     * @var string
     */
    private $command;

    /**
     * Path of the log file.
     * @var string
     */
    private $logFile;

    /**
     * Redirect stderr to log file?
     * @var bool
     */
    private $redirectStderr = true;

    /**
     * The working directory for this program.
     * @var string
     */
    private $workingDirectory;

    /**
     * Is this configuration file for a daemon?
     * @var bool
     */
    private $isDaemon = false;

    /**
     * The user to run program as.
     * @var string
     */
    private $user = 'www-data';

    /**
     * The number of processes to run.
     * @var int
     */
    private $numberOfProcesses = 1;

    /**
     * The path of the supervisor configuration directory.
     * @var string
     */
    private $supervisorConfDir = "/etc/supervisord/conf/";

    /**
     * @param string $supervisorConfDir
     */
    public function setSupervisorConfDir($supervisorConfDir)
    {
        $this->supervisorConfDir = $supervisorConfDir;
    }

    /**
     * Create a new configuration file generator.
     * @param $command
     * @param $logFile
     * @param $name
     * @param $redirectStderr
     * @param $workingDirectory
     */
    function __construct($command, $logFile, $name, $redirectStderr, $workingDirectory)
    {
        $this->command = $command;
        $this->logFile = $logFile;
        $this->name = $name;
        $this->redirectStderr = $redirectStderr;
        $this->workingDirectory = $workingDirectory;
    }

    /**
     * Get the command to run.
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * Set the command to run.
     * @param string $command
     */
    public function setCommand($command)
    {
        $this->command = $command;
    }

    /**
     * Returns the number of processes to spawn.
     * @return int
     */
    public function getNumberOfProcesses()
    {
        return $this->numberOfProcesses;
    }

    /**
     * Sets the number of processes to spawn.
     * @param int $numberOfProcesses
     */
    public function setNumberOfProcesses($numberOfProcesses)
    {
        $this->numberOfProcesses = $numberOfProcesses;
    }

    /**
     * Returns the user that this program will run as.
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Sets the user to run this program as.
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Get the path to the log file.
     * @return string
     */
    public function getLogFile()
    {
        return $this->logFile;
    }

    /**
     * Set the path to the log file.
     * @param string $logFile
     */
    public function setLogFile($logFile)
    {
        $this->logFile = $logFile;
    }

    /**
     * Get the name of the program.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the name of the program.
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get whether to redirect stderr to log file.
     * @return boolean
     */
    public function getRedirectStderr()
    {
        return $this->redirectStderr;
    }

    /**
     * Return the working directory.
     * @return mixed
     */
    public function getWorkingDirectory()
    {
        return $this->workingDirectory;
    }

    /**
     * Sets the working directory.
     * @param mixed $workingDirectory
     */
    public function setWorkingDirectory($workingDirectory)
    {
        $this->workingDirectory = $workingDirectory;
    }

    /**
     * Set whether to redirect stderr to log file.
     * @param boolean $redirectStderr
     */
    public function setRedirectStderr($redirectStderr)
    {
        $this->redirectStderr = $redirectStderr;
    }

    /**
     * Return whether this configuration file is for a daemon.
     * @return bool
     */
    public function isDaemon()
    {
        return $this->isDaemon;
    }

    /**
     * Set whether this configuration file is for a daemon.
     * @param $isDaemon
     */
    public function setDaemon($isDaemon)
    {
        $this->isDaemon = $isDaemon;
    }

    /**
     * Generates a new configuration file.
     * @param bool $return
     */
    public function generate($return = false)
    {
        $config = array();
        $config[] = "[program:{$this->name}]";
        $config[] = "command={$this->command}";
        $config[] = "directory={$this->workingDirectory}";
        $config[] = "stdout_logfile={$this->logFile}";
        $config[] = "redirect_stderr=" . ($this->redirectStderr ? 'true' : 'false');

        if ($this->isDaemon) {
            if (trim($this->user) == "" || $this->numberOfProcesses <= 0) {
                throw new InvalidArgumentsException("Invalid arguments specified for daemon config");
            }
            $config[] = "process_name=%(program_name)s-%(process_num)01d";
            $config[] = "user={$this->user}";
            $config[] = "numprocs={$this->numberOfProcesses}";
            $config[] = "numprocs_start=1";
        }

        $contents = implode(PHP_EOL, $config);
        if (!$return) {
            $path = $this->getConfigurationFilePath();
            if (!File::isWritable($path)) {
                throw new FileNotWritableException("Could not write file {$path}");
            }
            return File::put($path, $contents);
        }
        return $contents;
    }

    /**
     * Return the supervisor configuration file path for this Generator instance.
     * @return string
     */
    private function getConfigurationFilePath()
    {
        return "{$this->supervisorConfDir}{$this->name}.conf";
    }

    /**
     * Load the configuration file back into this Generator object.
     */
    public function load()
    {
        // @TODO
    }

}